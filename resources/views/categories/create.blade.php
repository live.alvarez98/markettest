@extends('template.main')

@section('title')
Categories
@endsection

@section('content')
<form action="{{route('categories.store')}}" method="POST">
    @csrf
    <div class="d-flex justify-content-center">
    <div class="mb-3">
      <label for="product_name" class="form-label">Nombre de producto</label>
      <input type="text" class="form-control" id="product_name" name="name" >
     
    </div>
</div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
@endsection