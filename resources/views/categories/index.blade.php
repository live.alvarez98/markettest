@extends('template.main')

@section('title')
Categories
@endsection

@section('content')
<div class="row">
    <table class="table caption-top">
        <div class="col-12 col-sm-10">
            <h1>Categories</h1>
         </div>
        <div class="col-12 col-sm-2">
            <a href="{{route('categories.create')}}"   class="btn btn-primary">Agregar</a>
        </div>
    </div>
    <div class="row">
        <div class="col">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Actions</th>

        </tr>
        </thead>
        <tbody>
        @forelse ($categorias as $category)
        <tr>
           
            <td> {{$category->id}}</td>
            <td>{{$category->name}}</td>
            <input type="hidden" id="ide" name="ide" value="{{$category->id}}">
        
            <td><a href="/categories/delete/{{$category->id}}"   class="btn btn-danger">Eliminar</a>
            <a href="/categories/edit/{{$category->id}}"   class="btn btn-primary">Editar</a><td>
          

            
         
        </tr>
        @empty
      
        @endforelse

        </tbody>
    </div>
</div>
    </table>
@endsection