@extends('template.main')

@section('title')
Vendors
@endsection

@section('content')
<form action="/vendors/update/{{$vendor->id}}" method="POST">
    @csrf
    <div class="d-flex justify-content-center">
    <div class="mb-3">
      
      <label for="vendor_name" class="form-label">Nombre del vendedor</label>
      <input type="text" class="form-control" id="vendor_name" name="name" value="{{$vendor->name}}" >
      
     
    </div>
</div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
@endsection