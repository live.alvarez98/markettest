@extends('template.main')

@section('title')
Vendors
@endsection

@section('content')
<div class="row">
    <table class="table caption-top">
        <div class="col-12 col-sm-10">
            <h1>Vendors</h1>
         </div>
        <div class="col-12 col-sm-2">
            <a href="{{route('vendors.create')}}"   class="btn btn-primary">Agregar</a>
        </div>
    </div>
    <div class="row">
        <div class="col">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Actions</th>

        </tr>
        </thead>
        <tbody>
        @forelse ($vendors as $vendor)
        <tr>
           
            <td> {{$vendor->id}}</td>
            <td>{{$vendor->name}}</td>
            <input type="hidden" id="ide" name="ide" value="{{$vendor->id}}">
        
            <td><a href="/vendors/delete/{{$vendor->id}}"   class="btn btn-danger">Eliminar</a>
            <a href="/vendors/edit/{{$vendor->id}}"   class="btn btn-primary">Editar</a><td>
          

            
         
        </tr>
        @empty
      
        @endforelse

        </tbody>
    </div>
</div>
    </table>
@endsection