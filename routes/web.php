<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\VendorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/categories', [CategoryController::class, 'index'])->name('categories.index');
Route::get('/categories/create', [CategoryController::class, 'create'])->name('categories.create');
Route::get('/categories/delete/{id}', [CategoryController::class, 'delete'])->name('categories.delete');
Route::get('/categories/edit/{id}', [CategoryController::class, 'edit'])->name('categories.edit');

Route::post('/categories', [CategoryController::class, 'store'])->name('categories.store');
Route::post('/categories/update/{id}', [CategoryController::class, 'update'])->name('categories.update');


Route::get('/vendors', [VendorController::class, 'index'])->name('vendors.index');
Route::get('/vendors/create', [VendorController::class, 'create'])->name('vendors.create');
Route::get('/vendors/delete/{id}', [VendorController::class, 'delete'])->name('vendors.delete');
Route::get('/vendors/edit/{id}', [VendorController::class, 'edit'])->name('vendors.edit');

Route::post('/vendors', [VendorController::class, 'store'])->name('vendors.store');
Route::post('/vendors/update/{id}', [VendorController::class, 'update'])->name('vendors.update');