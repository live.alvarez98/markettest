<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vendor;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
     
        
        $vendors = Vendor::get();

        return view('vendors.index')->with('vendors',$vendors);
    }

    public function store(Request $request){

        $vendor = new Vendor;
        $vendor->name = $request->name;
        $vendor->save();
        
        return redirect()->route('vendors.index');

    }

    public function create(){

        return view('vendors.create');
    }

    public function edit($id){
        
        $vendors=Vendor::findOrFail($id);
        
        return view('vendors.edit')->with('vendor',$vendors);
      
    }

    public function update(Request $request,$id){
        $vendor = Vendor::findOrFail($id);
        $vendor->name = $request->name;
        $vendor->save();
        
        return redirect()->route('vendors.index');

    }

    public function delete($id){
      
    
        $vendor=Vendor::find(intval($id));
        
        $vendor->delete();

        return redirect()->route('vendors.index');
        

        
       
    }
}
