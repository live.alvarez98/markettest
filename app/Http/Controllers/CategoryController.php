<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index(){
        //$category = Category::create(['name'=>'Verduras']);

        //$category = new Category;
       
        //$category->name = 'Frutas';
        //$category->save();
        
        $categories=Category::get();

        return view('categories.index')->with('categorias',$categories);
    }
    public function store(Request $request){

        $category = new Category;
        $category->name = $request->name;
        $category->save();
        
        return redirect()->route('categories.index');

    }

    public function create(){

        return view('categories.create');
    }

    public function edit($id){
        
        $categories=Category::findOrFail($id);
        
        return view('categories.edit')->with('category',$categories);
      
    }

    public function update(Request $request,$id){
        $category = Category::findOrFail($id);
        $category->name = $request->name;
        $category->save();
        
        return redirect()->route('categories.index');

    }

    public function delete($id){
      
    
        $category=Category::find(intval($id));
        
        $category->delete();

        return redirect()->route('categories.index');
        

        
       
    }
}
    
